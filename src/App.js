import React, { Component } from "react";
import { connect } from "react-redux";
import { Route, withRouter } from "react-router-dom";
import { push } from "connected-react-router";
import Demo1 from "./components/Demo1"
import { demo1 } from "./redux/action"

const Shopping = () => <p>Shopping</p>;
const Announce = () => <p>Announce</p>;

class App extends Component {
  goTo = route => {
    this.props.dispatch(push(route.url));
  };

  aa = () => {
    this.props.dispatch(demo1(100));
    console.log('test', this.props)
  }

  render() {
    console.log('app', this.props)
    return (
      <div>
        <ul>
          <li>
            Home
           
          </li>
          <li>
            Shopping
          </li>
          <li>
           Demo1
          </li>
        </ul>

        <div>
          <button onClick={() => this.aa()}>
          Click test change state Redux!
          </button>
          <button onClick={() => this.goTo({ url: "/demo1" })}>
                Click here to go to Demo1!
            </button>
        </div>

        <div style={{ padding: "150px" }}>
          <Route exact path="/" component={Announce} />
          <Route path="/shopping" component={Shopping} />
          <Route path="/demo1" component={Demo1} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = function(store) {
  return {
      location: store.router.location,
      test1Reducer: store.test1Reducer
  }
};

export default withRouter(connect(mapStateToProps)(App));
