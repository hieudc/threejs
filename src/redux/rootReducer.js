import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import test1Reducer from './test1Reducer'
 
export default (history) => combineReducers({
  router: connectRouter(history),
  test1Reducer
});