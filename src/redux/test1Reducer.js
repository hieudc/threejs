import { TES1 } from './actionType';

const initialState = {
    demo: 1
}

export default function test1Reducer(state = initialState, action) {
    switch (action.type) {
        case TES1:
            return Object.assign({}, state, {
                demo: action.value
            })
        default:
            return state;
    }
}