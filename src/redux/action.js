import { TES1 } from './actionType';

export function demo1(value) {
    return {
        type: TES1,
        value: value
    }
}