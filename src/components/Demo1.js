import React, { Component } from 'react';
import { push } from "connected-react-router";
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import { demo1 } from '../redux/action';

class Demo1 extends Component {
    constructor(props, context) {
        super(props, context);
        
        console.log(this.props)
    }

    goTo = route => {
        this.props.dispatch(push(route.url));
    };

    testRedux = value => {
        this.props.dispatch(demo1(value));
    }

    render() {
        //console.log(store)
        return(
        <div>Here is Demo1!
            <button onClick={() => this.goTo({ url: "/shopping" })}>
                Click here to go to Shooping!
            </button>
            <button onClick={() => this.testRedux(4)}>
                Click test change state Redux!
            </button>
        </div>
        )
    }
}

const mapStateToProps = function(store, state) {
    return {
        location: store.router.location,
        test1Reducer: store.test1Reducer
    }
};
  
export default withRouter(connect(mapStateToProps)(Demo1));
